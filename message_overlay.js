

export default class MessageOverlay {
    constructor() {
        // Create elements of overlay
        this._overlay = document.createElement("DIV");
        this._overlay.setAttribute("class", "message-overlay");
        this._msgbox = document.createElement("DIV");
        this._spinner = document.createElement("DIV");
        this._spinner.setAttribute("class", "loader");
        this._spinner.style.display = "none";
        this._overlay.appendChild(this._spinner);
        this._msgbox.setAttribute("class", "message-overlay-box");
        this._msgbox.innerText = "Test";
        this._msgbox.style.display = "none";
        this._overlay.appendChild(this._msgbox);
        var myObj = this;
        this._overlay.addEventListener("click", function () {
            myObj._overlay.style.display = "none";
            myObj._spinner.style.display = "none";
            myObj._msgbox.style.display = "none";
        });
    }

    ShowSpinner() {
        this._overlay.style.display = "block";
        this._spinner.style.display = "block";
    }

    HideSpinner() {
        this._overlay.style.display = "block";
        this._spinner.style.display = "none";
    }

    ShowMessage(msg) {
        this.HideSpinner();
        this._msgbox.innerText = msg;
        this._msgbox.style.display = "block";
    }

    ShowError(error) {
        this.HideSpinner();
        this._msgbox.innerText = error;
        this._msgbox.style.display = "block";
    }

    GetOverlay() {
        return this._overlay;
    }
}