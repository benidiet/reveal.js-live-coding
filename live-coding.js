/*
 * Reveal.js live coding plugin
 * MIT licensed
 * (c) Benedikt Dietrich 2020
 */

import config from './config.js';
import {CProgram, CppProgram, PythonProgram, WandBoxAPI} from './wandbox_api.js';
import MessageOverlay from './message_overlay.js'

const Plugin = () => {

    let deck;

    function ProgramFactory(classlist, isSnippet) 
    {
        if (classlist.contains('c') || classlist.contains('C')) 
        {
            return new CProgram(isSnippet);
        } 
        else if ( classlist.contains('c++') 
               || classlist.contains('C++') 
               || classlist.contains('cpp') 
               || classlist.contains('CPP')) 
        {
            return new CppProgram(isSnippet);
        }
        else if (classlist.contains('python') 
              || classlist.contains('PYTHON')
              || classlist.contains('Python')
            )
        {
            return new PythonProgram(isSnippet);
        } 
        else 
        {
            error("Unknown language");
        }
    }

    function selectElementContents(el) {
        var range = document.createRange();
        range.selectNodeContents(el);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }

    function codeKeyHandler(evt) {
        if (evt.key === "Tab") {
            evt.preventDefault();
            var sel = window.getSelection();
            var range = sel.getRangeAt(0);
            var tabNode = document.createTextNode("\u00a0\u00a0\u00a0\u00a0");
            range.insertNode(tabNode);
            range.setStartAfter(tabNode);
            range.setEndAfter(tabNode); 
            sel.removeAllRanges();
            sel.addRange(range);
        }
    }

    function findAncestor (el, sel) {
        while ((el = el.parentElement) && !((el.matches || el.matchesSelector).call(el,sel)));
        return el;
    }

    function openLiveCoding() {
        var elements = document.querySelectorAll(".live-coding, .live-coding-snippet, .live-coding-multi");

        elements.forEach(function (element) {

            var msgOverlay = new MessageOverlay();
            var section = findAncestor(element, "section");
            section.appendChild(msgOverlay.GetOverlay());

            var api;                  

            if (config.api === "WandBox") {
                api = new WandBoxAPI();
                api.oncompilererror = function(msg) {
                    msgOverlay.ShowError(msg);
                }
                api.onexecutionresult = function(stdout, stderr, returnCode) {
                    if (stderr != "") {
                        msgOverlay.ShowMessage("Program terminated with error!\n" + "\n\nOutput:\n" + stdout + "\n\nError:" + stderr + "\n\nReturn code: " + String(returnCode));
                    } else {
                        var msg = "";
                        if (stdout != "") {
                            msg += "Program output:\n" + stdout;
                        }
                        msg += "\nProgram return code: " + returnCode + "\n";
                        msgOverlay.ShowMessage(msg);
                    }
                }
                api.onnetworkerror = function() {
                    msgOverlay.ShowError("Error sending compile request.");
                }
            } else {
                console.log("Live-Coding Plugin: Requested API not defined!");
            }

            // Getting properties from page
            var isSnippet = element.classList.contains("live-coding-snippet");
            var program;

            var runButton = document.createElement("BUTTON");
            runButton.innerHTML = "Run";

            if (element.classList.contains("live-coding-multi")) {
                var files = element.querySelectorAll("[data-filename]");

                // Generate tab buttons
                var tabgroup = document.createElement("DIV");
                tabgroup.setAttribute("class", "live-coding-tab");

                // Generate Tab and Run buttons
                var tabbuttons = [];
                var isFirst = true;
                files.forEach(function (file) {

                    file.setAttribute('class', 'live-coding-tabcontent');

                    // Create a button for each file
                    var tabbutton = document.createElement("BUTTON");
                    tabbutton.setAttribute("class", "live-coding-tablinks");
                    tabbutton.innerHTML = file.getAttribute("data-filename");
                    tabbuttons.push(tabbutton);

                    // Allow switching between tabs on click
                    tabbutton.addEventListener("click", function () {
                        // Update visibility of code
                        for (var i = 0; i < files.length; i++) {
                            files[i].style.visibility = "hidden";
                        }
                        file.style.visibility = "visible";

                        // update active state of buttons
                        tabbuttons.forEach(function (tabButton) {
                            tabButton.removeAttribute("active");
                        });
                        this.setAttribute("active", "active");
                    });

                    // Make filename editable by double-clicking
                    tabbutton.addEventListener("dblclick", function () {
                        this.contentEditable = "true";
                        selectElementContents(this);
                    });
                    tabbutton.addEventListener("focusout", function () {
                        this.contentEditable = "false";
                    });
                    tabgroup.appendChild(tabbutton);

                    // First tab should be visible and active at beginning
                    if (isFirst) {
                        file.style.visibility = "visible";
                        isFirst = false;
                        tabbutton.setAttribute("active", "active");
                    } else {
                        file.style.visibility = "hidden";
                    }
                });

                // Make all code environments editable
                var codeEnvironments = element.querySelectorAll("code");
                for (var codeEnv of codeEnvironments) {
                    codeEnv.setAttribute("contenteditable", "true");
                    codeEnv.setAttribute("spellcheck", "false");
                    codeEnv.addEventListener("keydown", codeKeyHandler);
                }

                // Determine programming language and get program object
                var program = ProgramFactory(element.classList, isSnippet);

                // Configure run-button
                runButton.setAttribute("class", "live-coding-tablinks run-button");
                runButton.style.float = "right";
                runButton.addEventListener("click", function () {
                    program.Clear();
                    var filetabs = element.querySelectorAll("[data-filename]");
                    filetabs.forEach(function (file) {
                        program.AddCodeFile(file.getAttribute("data-filename"), file.textContent);
                    });
                    msgOverlay.ShowSpinner();
                    api.compileAndRun(program);
                });

                tabgroup.appendChild(runButton);
                element.insertBefore(tabgroup, element.firstChild);
            } else {
                element.appendChild(runButton);

                var code = element.querySelector("code");
                code.setAttribute("contenteditable", "true");
                code.setAttribute("spellcheck", "false");
                code.addEventListener("keydown", codeKeyHandler);

                program = ProgramFactory(element.classList, isSnippet);

                runButton.style = "position:absolute; right:0; top:0;";
                runButton.addEventListener("click", function () {
                    program.Clear();
                    program.AddCodeFile("", code.innerText);
                    msgOverlay.ShowSpinner();
                    api.compileAndRun(program);
                });
            }
        }); // for each live-coding
    }

    return {
        id: 'LiveCoding',

        init: function( reveal ) {

            deck = reveal;

            openLiveCoding();
        },

        open: openLiveCoding
    };

};

export default Plugin;
