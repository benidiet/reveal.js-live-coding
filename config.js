export default 
{
    "api": "WandBox", // Options: 
                    // "WandBox", 
                    // "Paiza" (ccurently under construction...)]

    "csnippet": {
        "head": [
            "#include <stdio.h>\n" +
            "#include <stdlib.h>\n" +
            "#include <inttypes.h>\n" +
            "#include <string.h>\n" +
            "\n" +
            "int main() {\n"
        ],
        "foot": [
            "\n}\n"
        ]
    },

    "cppsnippet": {
        "head": [
            "#include <stdio.h>\n"      + 
            "#include <stdlib.h>\n"     + 
            "#include <inttypes.h>\n"   +
            "#include <string.h>\n"     +
            "#include <string>\n"       +
            "#include <sstream>\n"      +
            "#include <vector>\n"       +
            "#include <list>\n"         +
            "#include <iostream>\n"     +
            "#include <iomanip>\n"      +
            "#include <cmath>\n"        +
            "\n"                        +
            "int main() {\n"
        ],
        "foot": [
            "\n}\n"
        ],
    },

    "pythonsnippet" : {
        "head": [
            ""
        ],
        "foot": [
            ""
        ]
    },

    "pythonprintalways" : {
        "head": [
            "import ast\n" + 
            "class ExpressionPrinter(ast.NodeTransformer):\n" + 
            "    visit_ClassDef = visit_FunctionDef = lambda self, node: node\n" + 
            "    def visit_Expr(self, node):\n" + 
            "        node = ast.copy_location(\n" + 
            "            ast.Expr(\n" + 
            "                ast.Call(ast.Name('print', ast.Load()),\n" + 
            "                         [node.value], [])\n" + 
            "                ),\n" + 
            "            node\n" + 
            "        )\n" + 
            "        ast.fix_missing_locations(node)\n" + 
            "        return node\n" + 
            "\n" + 
            "src = \'\'\'\n"
        ],
        "foot": [
            "'''\n" +
            "def main():\n" +
            "    tree = ast.parse(src, mode='exec')           \n" +
            "    new_tree = ExpressionPrinter().visit(tree)\n" +
            "    exec(compile(new_tree, filename=\"ast\", mode='exec'))\n" +
            "if __name__ == '__main__':\n" +
            "    main()\n"
        ]
    }
}