/*
 * Reveal.js live coding plugin
 * MIT licensed
 * (c) Benedikt Dietrich 2020
 */
import config from './config.js';

class Program {
    constructor(language) {
        this.language = language;
        this.compilerConfiguration = {
            compiler: "",
            options: "",
            optionraw: ""
        }
        this.codes = [];
        this.snippet_head = "";
        this.snippet_foot = "";
        this.stdin = "";
    }

    Clear() {
        this.codes = [];
    }

    AddCodeFile(filename, code) {
        code = this.snippet_head + code + this.snippet_foot;
        this.codes.push({ filename: filename, code: code });
    }
}

export class CProgram extends Program {
    constructor(isSnippet) {
        super("c");

        this.compilerConfiguration = {
            compiler: "gcc-head-c",
            options: "-Wall -Wextra -std=gnu11",
            optionraw: ""
        }

        if (isSnippet) {
            this.snippet_head = config.csnippet.head;
            this.snippet_foot = config.csnippet.foot;
        }
    }

    AddCodeFile(filename, code) {
        if (filename === "") {
            filename = 'main.c';
        }
        super.AddCodeFile(filename, code);
    }
}

export class CppProgram extends Program {
    constructor(isSnippet) {
        super("cpp");

        this.compilerConfiguration = {
            compiler: "gcc-head",
            options: "warning,boost-1.71.0-gcc-head,gnu++2a,cpp-no-pedantic",
            optionraw: ""
        }

        if (isSnippet) {
            this.snippet_head = config.cppsnippet.head;
            this.snippet_foot = config.cppsnippet.foot;
        }
    }

    AddCodeFile(filename, code) {
        if (filename === "") {
            filename = 'main.cpp';
        }
        super.AddCodeFile(filename, code);
    }
}

export class PythonProgram extends Program {
    constructor(isSnippet) {
        super("Python");

        this.compilerConfiguration = {
            compiler: "cpython-3.10.2",
            options: "",
            optionraw: ""
        }

        if (isSnippet) {
            this.snippet_head = config.pythonsnippet.head;
            this.snippet_foot = config.pythonsnippet.foot;
        }
    }

    AddCodeFile(filename, code) {
        if (filename === "") {
            filename = 'main.py';
        }
        super.AddCodeFile(filename, code);
    }
}

/**
 * This code integrates WandBox to do the online 
 * compilation & execution. 
 */
export class WandBoxAPI {
    constructor(ui) {
        this._ui = ui;
        this._host = "https://wandbox.org/api";
        this._endpoint_compile = "/compile.json";
        this.onnetworkerror = function () {
            console.log("Error sending compile request. Are you online?");
        }
        this.oncompilererror = function (msg) {
            console.log("Compiler error: " + msg);
        }
        this.onexecutionresult = function (stdout, stderr, returnCode) {
            console.log("Program stdout: " + stdout);
            console.log("Program stderr: " + stderr);
            console.log("Program return code: " + String(returnCode));
        }
    }

    programToPayload(program) {
        var payload;
        var compilerOptionsRaw = "";

        var otherFiles = [];
        for (var i = 1; i < program.codes.length; i++) {
            otherFiles.push({ file: program.codes[i].filename, code: program.codes[i].code });
        }

        if (program.language === "cpp") {
            for (var i = 1; i < program.codes.length; i++) {
                if (program.codes[i].filename.split('.').pop() == "cpp") {
                    compilerOptionsRaw += program.codes[i].filename + "\n";
                }
            }
        }
        
        if (program.language === "c") {
            for (var i = 1; i < program.codes.length; i++) {
                if (program.codes[i].filename.split('.').pop() == "c") {
                    compilerOptionsRaw += program.codes[i].filename + "\n";
                }
            }
        }

        payload = {
            compiler: program.compilerConfiguration.compiler,
            code: program.codes[0].code,
            codes: otherFiles,
            stdin: "",
            options: program.compilerConfiguration.options,
            "compiler-option-raw": compilerOptionsRaw,
            "runtime-option-raw": ""
        };

        return payload;
    }

    compileAndRun(program) {
        var xhr = new XMLHttpRequest();
        var url = this._host + this._endpoint_compile;
        var self = this;
        xhr.onreadystatechange = function () {
            self.handleCompileRequestResponse(xhr);
        };

        var payload = this.programToPayload(program);

        var str = JSON.stringify(payload);
        
        // TODO: xhr.timeout = 10000; 
        
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
        xhr.send(str);
    }

    handleCompileRequestResponse(xhr) {
        var response;
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                response = JSON.parse(xhr.responseText);

                /** 
                 * C/C++:
                 * - write to stdout -> program_output
                 * - Compiler/Linker warning but return 0: program_output, status=0
                 * - Compiler/Linker warning but return != 0: program_output, status
                 * - Compiler/Linker error (no exec): compiler_error, compiler_message, status=1
                 * - write to stderr -> program_error
                 * 
                 * Python: 
                 * - Syntax error: program_error contains error; status = 1
                 * - Execution error: 
                 */
                if (response.hasOwnProperty("compiler_error") && response.compiler_error != "") {
                    this.oncompilererror(response.compiler_error);
                }
                else {
                    if (! response.hasOwnProperty("program_output")) {
                        response.program_output = "";
                    }
                    if (! response.hasOwnProperty("program_error")) {
                        response.program_error = "";
                    }

                    this.onexecutionresult(response.program_output, 
                                           response.program_error, 
                                           response.status);
                }
            } else if (xhr.status === 0) {
                this.onnetworkerror();
            }
        }
    }
}

