# reveal.js-live-coding

A plugin to edit, compile and run source code from within the presentation. Currently supported languages are C/C++ and Python. 

[Check out the live demo](https://lecturetemplate.gitlab.io/lectureslides/8be3c943b1609fffbfc51aad666d0a04adf83c9d/live_coding/index.html#/).

## Installation

### Manual

Just clone this repository into your reveal.js presentation. 

Import the CSS file:

~~~ html
    <link rel="stylesheet" href="path/to/plugin/live-coding.css">
~~~

Add the plugin as module (supported since Reveal.js 4.0):

~~~ html
<script type="module">
    import Reveal from 'dist/reveal.esm.js';

    import LiveCoding from 'path/to/plugin/live-coding.js';

    Reveal.initialize({
        plugins: [ LiveCoding ]
    });
</script>
~~~

### Bower

TBD

### npm

TBD

## Configuration

You can configure the code that shall be added before (head) and after (foot) code snippets for the different programming languages in the config.js file.

## Usage

The plugin offers three different live-coding environments.

### Simple Environment

The simple environment creates a text box that can be edited. The whole content will be put in one file which will be uploaded, compiled and executed. 

Example:

```html
<pre live-coding='simple'><code class='C++' data-trim>
    #include &lt;iostream&gt;
    
    int main() {
        std::cout &lt;&lt; "Hello World!" &lt;&lt; std::endl;
    }
</code></pre>
```

### Code Snippet Environment

The code snippet environment automatically adds code (specified in config.js) before the snippet code and after the snippet code, before it uploads the resulting file for compilation and execution.

Example:

```html
<pre live-coding='snippet'><code class='C++' data-trim>
    std::cout &lt;&lt; "Hello World!" &lt;&lt; std::endl;
</code></pre>
```

### Multi-File Environment

The multi-file environment allows splitting the source code accross multiple files. The plugin collects the content of all tabs and uploads it for compilation and execution.

Example:

```html
<div live-coding="multi">
    <pre filename="main.cpp"><code class='C++' data-trim>
    #include "helper.h"
    #include &lt;iostream&gt;

    int main() {
        std::cout &lt;&lt; helperFunction() &lt;&lt; std::endl;
    }
    </code></pre>
    <pre filename="helper.h"><code class='C++' data-trim>
    #ifndef _HELPER_H_
    #define _HELPER_H_

    #include &lt;string&gt;

    std::string helperFunction() {
        return std::string("Hello World");
    }

    #endif 
    </code></pre>
</div>
```


## License

MIT licensed

Copyright (C) 2020 Benedikt Dietrich